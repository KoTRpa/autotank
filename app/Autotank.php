<?php
/**
* Автотанк
* 
* Умеет двигаться по прямой на расстояние n с помощью метода move(n)
* и поворачивать по часовой стрелке на 90 градусов методом turn()
*
* @package  Forcode\Autotank
* @author   Maxim Kashkin <4kotrpa@gmail.com>
* @version 0.0.1-alfa
*/

namespace ForCode\Autotank;

class Autotank
{
    protected $calculatedPath;

    /** @var int $direction текущее напрвление танка. может принимать значения 0, 90, 180, 270 */
    protected $direction;

    /** @var array местоположение автотанка */
    protected $position;

    /** @var array лог пройденного пути [[x, y, direction], ...] */
    protected $path;

    /**
     * Значения по-умолчанию:
     * direction - 90
     * position - [0, 0]
     * path - [[0, 0, 90]]
     */
    public function __construct()
    {
        $this->direction = 90;
        $this->position = [0, 0];

        $startPath = $this->position;
        $startPath[] = $this->direction;
        $this->path[] = $startPath;
    }

    public function moveTo($x, $y): Autotank
    {
        $this->destination = [$x, $y];

        $deltaX = $x - $this->position[0];
        $deltaY = $y - $this->position[1];


        if ($deltaX !== 0) {
            $turns = 0;
            if ($deltaX > 0) {
                if ($this->direction === 180) {
                    $turns = 3;
                } elseif ($this->direction === 270) {
                    $turns = 2;
                } elseif ($this->direction === 0) {
                    $turns = 1;
                }
            } else {
                if ($this->direction === 0) {
                    $turns = 3;
                } elseif ($this->direction === 90) {
                    $turns = 2;
                } elseif ($this->direction === 180) {
                    $turns = 1;
                }
            }

            $this->calculatedPath[] = $turns; // turns
            $this->calculatedPath[] = abs($deltaX); // move
        }

        if ($deltaY !== 0) {
            $turns = 0;
            if ($deltaY > 0) {
                if ($this->direction === 90) {
                    $turns = 3;
                } elseif ($this->direction === 180) {
                    $turns = 2;
                } elseif ($this->direction === 270) {
                    $turns = 1;
                }
            } else {
                if ($this->direction === 270) {
                    $turns = 3;
                } elseif ($this->direction === 0) {
                    $turns = 2;
                } elseif ($this->direction === 90) {
                    $turns = 1;
                }
            }

            $this->calculatedPath[] = $turns; // turns
            $this->calculatedPath[] = abs($deltaY); // move
        }

        return $this;
    }

    /**
     * @throws \Exception
     */
    public function start(): void
    {
        if (!is_array($this->calculatedPath) || count($this->calculatedPath) <= 0) {
            throw new \Exception('wrong calculated path');
        }

        foreach ($this->calculatedPath as $key => $value) {
            if ($key % 2 === 0) { // turn
                for ($i = 0; $i < $value; $i++) {
                    $this->turn();
                }
            } else { // move
                $this->move($value);
            }
        }

        $this->calculatedPath = null;
    }


    /**
     * Функция движения автотанка
     * 
     * @throws \Exception
     * 
     * @param int $distance расстояние которое должен пройти автотанк
     */
    public function move(int $distance): void
    {
        switch ($this->direction) {
            case 0:
                $this->position[1] += $distance;
                break;

            case 90:
                $this->position[0] += $distance;
                break;

            case 180:
                $this->position[1] -= $distance;
                break;

            case 270:
                $this->position[0] -= $distance;
                break;
            
            default:
                throw new \Exception('wrong direction value ' . $this->direction);
        }

        $this->logPath();
    }

    /**
     * Функция поворота автотанка
     * 
     * @return int направление в котором танк оказался после поворота (одно из значений 0, 90, 180, 270)
     */
    public function turn(): int
    {
        $this->direction += 90;

        if ($this->direction === 360) {
            $this->direction = 0;
        }

        $this->logPath();

        return $this->direction;
    }

    /**
     * Возвращает текущее местоположение
     * 
     * @return array
     */
    public function getPosition(): array
    {
        return $this->position;
    }

    /**
     * Возвращает лог пройденного пути
     * @return array
     */
    public function getPath(): array
    {
        return $this->path;
    }

    /**
     * Записывает в лог пройденнного пути path текущие местоположение и угол поворота
     * из атрибутов position и direction соответственно
     * Вызывается в методах move() и turn()
     */
    protected function logPath(): void
    {
        $path = $this->position;
        $path[] = $this->direction;
        $this->path[] = $path;
    }
}
