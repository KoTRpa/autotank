# Автотанк

## Условие

По заказу министерства обороны КБ "ПрямыеРуки" разработало танк с функцией автопилота.

В соответствие с техническими требованиями танк:

- Двигается по прямой на заданное расстояние
- Поворачивает посредством четырех рядовых
- Может повернуть только на 90 градусов и только по часовой стрелке, иное запрещено уставом

## Задача

Разработать алгоритм движения танка к заданным координатам X и Y с наименьшим количеством шагов.

Изначально танк находится в точке 0,0 и повернут в сторону положительной оси X

## Результат

Должен выглядеть как лог движения танка (№. X Y Направление):

1. 0 0 90
2. 5 0 90
3. 5 0 180
4. 5 0 270
5. 5 0 0
6. 5 7 0
7. ...
