<?php 

namespace Forcode\Autotank\Tests\Feature;

use PHPUnit\Framework\TestCase;

use ForCode\Autotank\Autotank;

class AutotankTest extends TestCase
{

    /**
     * @dataProvider moveInDirectionDataProvider
     */
    public function testMoveInDirection($turn, $distance, $expected)
    {
        $autotank = new Autotank();

        for ($i=0; $i < $turn; $i++) {
            $autotank->turn();
        }

        $autotank->move($distance);

        $this->assertSame($expected, $autotank->getPosition());
    }

    /**
     * each even value of path is turn
     * and odd is move
     * 
     * @dataProvider moveByPathDataProvider
     */
    public function testMoveByPath($path, $expected)
    {
        $autotank = new Autotank();

        foreach ($path as $key => $value) {
            if ($key % 2 == 0) {
                for ($i=0; $i < $value; $i++) {
                    $autotank->turn();
                }
            } else {
                $autotank->move($value);
            }
        }

        $this->assertSame($expected, $autotank->getPosition());
    }

    /**
     * each even value of path is turn
     * and odd is move
     * 
     * @dataProvider logPathDataProvider
     */
    public function testLogPath($path, $expected)
    {
        $autotank = new Autotank();

        foreach ($path as $key => $value) {
            if ($key % 2 == 0) {
                for ($i=0; $i < $value; $i++) {
                    $autotank->turn();
                }
            } else {
                $autotank->move($value);
            }
        }

        $this->assertSame($expected, $autotank->getPath());
    }

    //-----------------------------------------------------------------
    // Data Providers

    public function moveInDirectionDataProvider()
    {
        return [
            'positiveX' => ['turn' => 0, 'distance' => 1, 'expected' => [1, 0]],
            'positiveY' => ['turn' => 3, 'distance' => 1, 'expected' => [0, 1]],
            'negativeX' => ['turn' => 2, 'distance' => 1, 'expected' => [-1, 0]],
            'negativeY' => ['turn' => 1, 'distance' => 1, 'expected' => [0, -1]],
        ];
    }

    public function moveByPathDataProvider()
    {
        return [
            ['path' => [0, 5, 1, 3], 'expected' => [5, -3]],
            ['path' => [3, 2, 0, 3], 'expected' => [0, 5]],
            ['path' => [2, 5, 3, 3], 'expected' => [-5, -3]],
            ['path' => [0, 5, 3, 3], 'expected' => [5, 3]],
        ];
    }

    public function logPathDataProvider()
    {
        return [
            ['path' => [0, 5, 1, 3], 'expected' => [
                [0, 0, 90],
                [5, 0, 90],
                [5, 0, 180],
                [5, -3, 180]
            ]],
            ['path' => [3, 2, 0, 3], 'expected' => [
                [0, 0, 90], // initial
                [0, 0, 180], // turn
                [0, 0, 270], // turn
                [0, 0, 0], // turn
                [0, 2, 0], // move 2
                [0, 5, 0] // move 3
            ]],
            ['path' => [2, 5, 3, 3], 'expected' => [
                [0, 0, 90],
                [0, 0, 180],
                [0, 0, 270],
                [-5, 0, 270],
                [-5, 0, 0],
                [-5, 0, 90],
                [-5, 0, 180],
                [-5, -3, 180],
            ]],
            ['path' => [0, 5, 3, 3], 'expected' => [
                [0, 0, 90],
                [5, 0, 90],
                [5, 0, 180],
                [5, 0, 270],
                [5, 0, 0],
                [5, 3, 0]
            ]],
        ];
    }
}
