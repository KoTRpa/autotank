<?php 

namespace Forcode\Autotank\Tests\Unit;

use PHPUnit\Framework\TestCase;

use ForCode\Autotank\Autotank;

class AutotankTest extends TestCase
{
    /**
     * @covers Autotank::move
     * @dataProvider moveDataProvider
     */
    public function testMove($distance, $expected)
    {
        $autotank = new Autotank();

        $autotank->move($distance);

        $this->assertSame($expected, $autotank->getPosition());
    }

    /**
     * @covers Autotank::turn
     * @dataProvider turnDataProvider
     */
    public function testTurn($times, $expected)
    {
        $autotank = new Autotank();

        for ($i = 0; $i < $times; $i++) {
            $turned = $autotank->turn();
        }

        $this->assertSame($expected, $turned);
    }

    /**
     * @covers Autotank::getPath
     * @dataProvider turnDataProvider
     */
    public function testGetPath()
    {
        $autotank = new Autotank();

        $this->assertSame([[0, 0, 90]], $autotank->getPath());
    }


    //-----------------------------------------------------------------
    // Data Providers


    public function moveDataProvider()
    {
        return [
            ['distance' => 0, 'expected' => [0, 0]],
            ['distance' => 5, 'expected' => [5, 0]],
            ['distance' => 7, 'expected' => [7, 0]],
        ];
    }

    public function turnDataProvider()
    {
        return [
            ['times' => 1, 'expected' => 180],
            ['times' => 2, 'expected' => 270],
            ['times' => 3, 'expected' => 0],
            ['times' => 4, 'expected' => 90],
        ];
    }
}
