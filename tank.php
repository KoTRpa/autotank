<?php

require 'vendor/autoload.php';

use ForCode\Autotank\Autotank;

$autotank = new Autotank;

$autotank->moveTo(5, 3)->start();

$autotank->moveTo(5, 0)->start();

foreach ($autotank->getPath() as $key => $value) {
    echo $key, '. ', implode(' ', $value), PHP_EOL;
}
